export class AppRoutes {

  constructor($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');

      $stateProvider
        .state('home', {
          url: '/',
          templateUrl: '../views/template.html'

        })

      $locationProvider.html5Mode(true);
  }
}
