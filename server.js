let express = require('express');
let path = require('path');
let cors = require('cors');

let app = express();
let port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use(cors())

app.listen(port, () => {
  console.log("Dragons can be found at http://localhost:" + port);
})
